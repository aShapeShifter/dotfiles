# check the window size after each command and
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# all alias definitions in .bash_aliases
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# fancy prompt
blk='\e[0;30m' # Black
red='\e[0;31m' # Red
grn='\e[0;32m' # Green
ylw='\e[0;33m' # Yellow
blu='\e[0;34m' # Blue
pur='\e[0;35m' # Purple
cyn='\e[0;36m' # Cyan
wht='\e[1;37m' # White
def='\e[m'     # default color
PS1="[\[$grn\]\u\[$def\]@\[$grn\]\h\[$def\]:\[$red\]\W\[$def\]]\\$ "

# source file containing environment variables
source .profile
