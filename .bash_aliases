# application shortcuts
alias nv='nvim'

# ls
alias l='ls -lh --color=auto'
alias la='ls -alh --color=auto'

# fg
alias f='fg'

# cd
alias ..='cd ..'

# halt and reboot
alias h='poweroff'
alias r='poweroff --reboot'

# du, df, grep
alias du='du -h'
alias df='df -h'
alias grep='grep --color=auto'

# mount
alias mnt='sudo mount -o gid=users,fmask=113,dmask=002'
alias umnt='sudo umount'

# music
alias mp='mpd ~/.config/mpd/mpd.conf'
alias mc='ncmpcpp'
alias abc='abcde -N'
alias sq='squeezelite -n Sqeezelite_$HOSTNAME -z'

# package management
alias pn='sudo pacman -Suy --noconfirm'
alias yn='yay -Suy --noconfirm'
alias rm_unused_packages='sudo pacman -Rsn $(pacman -Qdtq); yay -Yc'

# virtualbox
alias win7='virtualbox --startvm win7 &'

# terminal
alias t='uxterm -bg black -fg white -bc -fn 10x20'

# git
alias g='git'
alias gst='while true; do clear; git status; read; done'
alias gmp='while true; do clear; git log -n 25 --graph --decorate --oneline --all; read; done'
alias G='t -e "while true; do clear; git log -n 40 --graph --decorate --oneline --all; read; done" & t -e "while true; do clear; git status; read; done" &'

# wine applications
alias ltspice='wine ~/.wine/drive_c/ltspice/scad3.exe'
alias gp7='playonlinux --run "Guitar Pro 7"'

# edit certain textfiles
alias todo='vim ~/notes/todo.md'
alias order='vim ~/notes/order.md'

# misc
alias bg_scale='feh --bg-scale'
alias bg_max='feh --bg-max'
alias calc="python -ic \"import math\""
alias dmesg="dmesg --color=always | less -R"
alias awesome-from-xfce="killall xfce4-panel && killall xfwm4 && awesome &"
alias neo="setxkbmap -layout de -variant neo"

##only work
function gd-ssh(){
    ssh \
        -oKexAlgorithms=+diffie-hellman-group1-sha1\
        -oHostKeyAlgorithms=+ssh-dss\
        -l root\
        ${1:-192.168.0.1}\
    }

function gd-scp(){
    scp \
        -oKexAlgorithms=+diffie-hellman-group1-sha1\
        -oHostKeyAlgorithms=+ssh-dss\
        -oUser=root\
        $1\
        ${2:-192.168.0.1}:/data
    }
alias serial='picocom -b 115200 /dev/ttyUSB0'
##end
