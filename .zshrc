# load modules
autoload -Uz colors && colors
autoload -Uz compinit promptinit

# history
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# completion look and feel
zstyle ':completion:*' completer _complete _ignored _approximate
zstyle :compinstall filename '/home/simon/.zshrc'
zstyle ':completion:*:commands' list-colors '=*=1;31'
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
compinit

# auto change directories
setopt autocd

# no beeps
unsetopt beep

# prompt
##only obelix
PROMPT="[%{$fg[red]%}%n%{$reset_color%}@%{$fg[cyan]%}%m%{$fg_no_bold[yellow]%}%{$reset_color%}] "
##end
##only asterix
PROMPT="[%{$fg[red]%}%n%{$reset_color%}@%{$fg[yellow]%}%m%{$fg_no_bold[blue]%}%{$reset_color%}] "
##end
##only troubadix
PROMPT="[%{$fg[red]%}%n%{$reset_color%}@%{$fg[green]%}%m%{$fg_no_bold[yellow]%}%{$reset_color%}] "
##end
##only idefix
PROMPT="[%{$fg[red]%}%n%{$reset_color%}@%{$fg[magenta]%}%m%{$fg_no_bold[yellow]%}%{$reset_color%}] "
##end
##only vm
PROMPT="[%{$fg[red]%}%n%{$reset_color%}@%{$fg[cyan]%}%m%{$fg_no_bold[yellow]%}%{$reset_color%}] "
##end
##only automatix
PROMPT="[%{$fg[yellow]%}%n%{$reset_color%}@%{$fg[red]%}%m%{$fg_no_bold[yellow]%}%{$reset_color%}] "
##end
##only methusalix
PROMPT="[%{$fg[green]%}%n%{$reset_color%}@%{$fg[blue]%}%m%{$fg_no_bold[blue]%}%{$reset_color%}] "
##end
##only SSteinkamp-T480
PROMPT="[%{$fg[green]%}%n%{$reset_color%}@%{$fg[blue]%}%m%{$fg_no_bold[blue]%}%{$reset_color%}] "
##end

# current working dir on the right side
RPROMPT="%{$fg[green]%}%d%{$reset_color%}"

# aliases and environment variables
source ~/.bash_aliases
source ~/.profile

# try to act a little bit like bash...
bindkey -e
bindkey '^P' up-history
bindkey '^N' down-history
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word
bindkey '^r' history-incremental-search-backward
bindkey ';5C' forward-word
bindkey ';5D' backward-word
bindkey '^[[3~' delete-char
bindkey '^[3;5~' delete-char
