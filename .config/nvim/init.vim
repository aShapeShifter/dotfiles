" syntax highlighting
colors desert
syn on

" indentation
set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

" autocomplete comments
set formatoptions+=r

" enable project specific local .vimrc configs
set exrc            " enable per-directory .vimrc files
set secure          " disable unsafe commands in local .vimrc files

" seach options
set hlsearch
set incsearch

" destination for swp files
set dir=~/tmp

"----------------------------------------------------------
" FUNCTIONS
"----------------------------------------------------------

" Remove trailing whitespace and whitespace in empty lines
function! ClearWhitespace()
    %s/\s\+$//e
    %s/^\s\+$//e
endfunction

"----------------------------------------------------------
" KEY MAPPINGS
"----------------------------------------------------------

" show/hide tree and outline
nmap <F2> :NERDTreeToggle<cr>
nmap <F3> :TagbarToggle<cr>

" normal mode
nmap <cr> :
nmap <space> i <esc>
nmap <bs> i<bs><esc>l

" function keys
map <F5> :!make<cr>

" windows
map <Up>                <C-w>k
map <Down>              <C-w>j
map <Left>              <C-w>h
map <Right>             <C-w>l

" tabs
map <Insert>            :tabnew<CR>:e
" ctrl+page up
map \E[6~                 :tabnext<CR>
" ctrl+page down
map \E[7~                 :tabprevious<CR>

"----------------------------------------------------------
" ACTION DEPENDING ON FILENAME EXTENSION
"----------------------------------------------------------
" VHDL files
autocmd BufRead,BufNewFile *.vhd set tabstop=3 shiftwidth=3
autocmd BufRead,BufNewFile *.vhd color default

" txt files
autocmd BufRead,BufNewFile *.txt set guifont=DejaVu\ Sans\ Mono\ 12
autocmd BufRead,BufNewFile *.txt color default

" yaml files
autocmd FileType yaml setlocal ts=2 sts=2 sw=2

" treat *.md as a markdown file
autocmd BufRead,BufNewFile *.md set filetype=markdown
autocmd BufRead,BufNewFile *.md color default

" tabs in makefiles
autocmd BufRead,BufNewFile [Mm]akefile set noexpandtab

" kicad
autocmd BufNewFile,BufRead *.kicad_mod,*.kicad_pcb,*.lib set filetype=lisp


"----------------------------------------------------------
" GVIM
"----------------------------------------------------------
" hide some gui stuff
set guioptions-=m
set guioptions-=T
set guioptions+=LlRrb
set guioptions-=LlRrb
