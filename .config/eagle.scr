#
# EAGLE EINSTELLUNGEN UND SHORTCUTS
#
# @file   ~/.config/eagle.scr
# @author Simon Steinkamp
#
#
 
##########################################################
# ALLGEMEIN
##########################################################

### GRID DEFINITIONEN ###
GRID = SchematicGrid mil 50 alt mil 25;
GRID = SymbolGrid mil 100 alt mil 50;
GRID = LayoutGrid mm 0.15 alt mm 0.05;

##########################################################
SCH:
##########################################################

### RESET ###
MENU '';

### STANDARD GROESSEN ###
CHANGE SIZE 50mil;
CHANGE RATIO 8;
CHANGE WIDTH 6mil; #wires

### GRID ###
GRID SchematicGrid;

### SHORTCUTS (Allgemein) ###
ASSIGN F2 'SHOW @';
ASSIGN Shift+F2 'SHOW';
ASSIGN F3 'GROUP';
ASSIGN F5 'GRID ON;';
ASSIGN Shift+F5 'GRID OFF;';
ASSIGN Shift+Ctrl+F5 'GRID';
ASSIGN F6 'MOVE';
ASSIGN F9 'WINDOW';
ASSIGN Shift+F9 'WINDOW FIT';

### SHORTCUTS ###
ASSIGN F7 'WIRE';
ASSIGN F8 'DELETE';

##########################################################
BRD:
##########################################################

### RESET ###
MENU '';

### GRID ###
GRID LayoutGrid;


### Vias ###
CHANGE SHAPE round;
CHANGE DRILL 0.3;

### LAYER DEFINITIONEN ###
DISPLAY = TopView None Dimension Unrouted 1 Pads Vias tPlace tOrigins tStop tDocu tKeepout tRestrict;
DISPLAY = BotView None Dimension Unrouted 16 Pads Vias bPlace bOrigins bStop bDocu bKeepout bRestrict;
DISPLAY = TopNames None Dimension tPlace tOrigins tNames tDocu;
DISPLAY = BotNames None Dimension bPlace bOrigins bNames bDocu;
DISPLAY = BotTest None Dimension bTest;
DISPLAY = ML16 None Dimension Unrouted 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 Pads Vias tPlace bPlace tOrigins bOrigins tDocu bDocu tStop bStop;
DISPLAY = ML16Routing None Dimension Unrouted 1 16 2 3 4 5 6 7 8 9 10 11 12 13 14 15 Pads Vias;
DISPLAY = MLDRC All -25 -26 -27 -28 -29 -30 -44 -45 -49;
DISPLAY = TBEST None Dimension tPlace tOrigins tNames tDocu; # Bestueckungsplan Top
DISPLAY = BBEST None Dimension bPlace bOrigins bNames bDocu; # Bestueckungsplan Bottom
DISPLAY = ROUTE2 None 2 Pads Vias Dimension Unrouted; # Innenlage Route2
DISPLAY = ROUTE3 None 3 Pads Vias Dimension Unrouted; # Innenlage Route3
DISPLAY = ROUTE4 None 4 Pads Vias Dimension Unrouted; # Innenlage Route4
DISPLAY = ROUTE5 None 5 Pads Vias Dimension Unrouted; # Innenlage Route5
DISPLAY = ROUTE6 None 6 Pads Vias Dimension Unrouted; # Innenlage Route6
DISPLAY = ROUTE7 None 7 Pads Vias Dimension Unrouted; # Innenlage Route7
DISPLAY = ROUTE8 None 8 Pads Vias Dimension Unrouted; # Innenlage Route8
DISPLAY = ROUTE9 None 9 Pads Vias Dimension Unrouted; # Innenlage Route9
DISPLAY = ROUTE10 None 10 Pads Vias Dimension Unrouted; # Innenlage Route10
DISPLAY = ROUTE11 None 11 Pads Vias Dimension Unrouted; # Innenlage Route11
DISPLAY = ROUTE12 None 12 Pads Vias Dimension Unrouted; # Innenlage Route12
DISPLAY = ROUTE13 None 13 Pads Vias Dimension Unrouted; # Innenlage Route13
DISPLAY = ROUTE14 None 14 Pads Vias Dimension Unrouted; # Innenlage Route14
DISPLAY = ROUTE15 None 15 Pads Vias Dimension Unrouted; # Innenlage Route15

### SHORTCUTS (Allgemein) ###
ASSIGN F2 'SHOW @';
ASSIGN Shift+F2 'SHOW';
ASSIGN F3 'GROUP';
ASSIGN F4 'DISPLAY last';
ASSIGN F5 'GRID ON;';
ASSIGN Shift+F5 'GRID OFF;';
ASSIGN Shift+Ctrl+F5 'GRID';
ASSIGN F6 'MOVE';
ASSIGN F9 'WINDOW';
ASSIGN Shift+F9 'WINDOW FIT';

### SHORTCUTS ###
# F-Tasten
ASSIGN F7 'ROUTE';
ASSIGN F8 'RIPUP';
# F10 .. F12: Ansichten
ASSIGN Shift+F10 'DISPLAY TBEST';
ASSIGN Ctrl+F10 'DISPLAY BBEST';
ASSIGN F11 'DISPLAY measures';
ASSIGN Shift+F11 'DISPLAY -measures';
ASSIGN F12 'DISPLAY ML16Routing;';
ASSIGN Shift+F12 'DISPLAY TopView;';
ASSIGN Ctrl+F12 'DISPLAY BotView;';
ASSIGN Shift+Ctrl+F12 'DISPLAY ML16';
ASSIGN Shift+Ctrl+Alt+F12 'DISPLAY MLDRC';

# Leiterbreiten (waehrend routing) aendern
ASSIGN Ctrl+1 'SET WIDTH 0.15';
ASSIGN Ctrl+2 'SET WIDTH 0.2';
ASSIGN Ctrl+3 'SET WIDTH 0.3';
ASSIGN Ctrl+4 'SET WIDTH 0.5';
ASSIGN Ctrl+5 'SET WIDTH 0.8';
ASSIGN Ctrl+6 'SET WIDTH 1';
ASSIGN Ctrl+7 'SET WIDTH 1.5';
ASSIGN Ctrl+8 'SET WIDTH 2';

### MENUELEISTE ###
MENU 'All: DISPLAY ALL;'\
     'TopView: DISPLAY TopView;'\
     'BotView: DISPLAY BotView;'\
     'TopNames: DISPLAY TopNames;'\
     'BotNames: DISPLAY BotNames;'\
     'ML16: DISPLAY ML16;'\
     'ML16Routing: DISPLAY ML16Routing;'\
     'DRC: DISPLAY MLDRC;'\
     'TBest: DISPLAY TBEST;'\
     'BBest: DISPLAY BBEST;'\
     'BTest: DISPLAY BotTest;'\
     ---\
     'R2: DISPLAY ROUTE2;'\
     'R3: DISPLAY ROUTE3;'\
     'R4: DISPLAY ROUTE4;'\
     'R5: DISPLAY ROUTE5;'\
     'R6: DISPLAY ROUTE6;'\
     'R7: DISPLAY ROUTE7;'\
     'R8: DISPLAY ROUTE8;'\
     'R9: DISPLAY ROUTE9;'\
     'R10: DISPLAY ROUTE10;'\
     'R11: DISPLAY ROUTE11;'\
     'R12: DISPLAY ROUTE12;'\
     'R13: DISPLAY ROUTE13;'\
     'R14: DISPLAY ROUTE14;'\
     'R15: DISPLAY ROUTE15;'\
     ---\
     'Ref AN: DISPLAY 49;'\
     'AUS: DISPLAY -49;'\
     ---\
     'Poly AN: SET polygon_ratsnest on; RATSNEST;'\
     'AUS: SET polygon_ratsnest off; RIPUP @;'\
     ---\
     'Unrouted: run zoom-unrouted.ulp;'\
     ---\
     'DXF: DISPLAY none 17 20 21 45 49; run dxf'\
     ---;

##########################################################
DEV:
##########################################################

### RESET ###
MENU '';

### GRID ###
GRID SchematicGrid;

### SHORTCUTS (Allgemein) ###
ASSIGN F2 'SHOW @';
ASSIGN Shift+F2 'SHOW';
ASSIGN F3 'GROUP';
ASSIGN F5 'GRID ON;';
ASSIGN Shift+F5 'GRID OFF;';
ASSIGN Shift+Ctrl+F5 'GRID';
ASSIGN F6 'MOVE';
ASSIGN F9 'WINDOW';
ASSIGN Shift+F9 'WINDOW FIT';

##########################################################
SYM:
##########################################################

### RESET ###
MENU '';

### GRID ###
GRID SymbolGrid;

### STANDARD GROESSEN ###
CHANGE SIZE 70mil; # >NAME, >VALUE
CHANGE RATIO 8;
CHANGE WIDTH 10mil; # Rahmen

### SHORTCUTS (Allgemein) ###
ASSIGN F2 'SHOW @';
ASSIGN Shift+F2 'SHOW';
ASSIGN F3 'GROUP';
ASSIGN F5 'GRID ON;';
ASSIGN Shift+F5 'GRID OFF;';
ASSIGN Shift+Ctrl+F5 'GRID';
ASSIGN F6 'MOVE';
ASSIGN F9 'WINDOW';
ASSIGN Shift+F9 'WINDOW FIT';

##########################################################
PAC:
##########################################################

### RESET ###
MENU '';

### STANDARD GROESSEN ###
# Strichbreite, zB Bestueckungsdruck:
CHANGE WIDTH 0.15mm;

### GRID ###
GRID LayoutGrid;

### SHORTCUTS (Allgemein) ###
ASSIGN F2 'SHOW @';
ASSIGN Shift+F2 'SHOW';
ASSIGN F3 'GROUP';
ASSIGN F5 'GRID ON;';
ASSIGN Shift+F5 'GRID OFF;';
ASSIGN Shift+Ctrl+F5 'GRID';
ASSIGN F6 'MOVE';
ASSIGN F9 'WINDOW';
ASSIGN Shift+F9 'WINDOW FIT';
