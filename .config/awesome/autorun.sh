#!/usr/bin/env bash


# function to run if not already running
function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

##only vm
##run /usr/bin/VBoxClient-all
##end
